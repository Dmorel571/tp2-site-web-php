<?php 
$dbHost = 'localhost';
$dbName = 'aec_agence';
$dbUser = 'root';
$dbPassword = '123456';
$dbCharset = 'utf8mb4';

$dsn = "mysql:host=$dbHost;dbname=$dbName;charset=$dbCharset";

$options = [
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, 
	PDO::ATTR_EMULATE_PREPARES   => false,
];

	try {
		// On se connecte à MySQL
		$bdd = new PDO($dsn, $dbUser, $dbPassword, $options);

	} 
	catch (PDOException $e) {
		// En cas d'erreur, on affiche un message et on arrête tout
		// throw new PDOException($e->getMessage(), (int)$e->getCode());
		die('Erreur : '.$e->getMessage());

	}

?>