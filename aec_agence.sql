-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 27, 2021 at 07:28 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aec_agence`
--

-- --------------------------------------------------------

--
-- Table structure for table `rental`
--

DROP TABLE IF EXISTS `rental`;
CREATE TABLE IF NOT EXISTS `rental` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `date_publication` date NOT NULL,
  `date_possession` date NOT NULL,
  `professional` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rental`
--

INSERT INTO `rental` (`id`, `address`, `image`, `price`, `date_publication`, `date_possession`, `professional`) VALUES
(1, 'rue des bouteilles', '1.jpg', '1220.00', '2021-08-10', '2021-08-26', 0),
(2, 'rue des feuilles', '2.jpg', '1400.00', '2021-07-05', '2021-08-18', 1),
(3, 'rue des trefles', '3.jpg', '1600.00', '2021-06-25', '2021-08-19', 0),
(4, 'rue des anciens', '4.jpg', '1770.00', '2021-06-24', '2021-08-18', 0),
(5, 'rue des grenouilles', '5.jpg', '1980.00', '2021-06-11', '2021-08-31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sell`
--

DROP TABLE IF EXISTS `sell`;
CREATE TABLE IF NOT EXISTS `sell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `date_publication` date NOT NULL,
  `condo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sell`
--

INSERT INTO `sell` (`id`, `address`, `image`, `price`, `date_publication`, `condo`) VALUES
(1, 'rue des escargots', 'un.jpg', '222000.00', '2021-08-26', 0),
(2, 'rue des chevreuils', 'deux.jpg', '300000.00', '2021-08-25', 0),
(3, 'rue des boutons', 'trois.jpg', '175000.00', '2021-08-10', 1),
(4, 'rue des sapphires', 'quatre.jpg', '400000.00', '2021-08-02', 0),
(5, 'rue des diamants', 'cinq.jpg', '368000.00', '2021-08-03', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
