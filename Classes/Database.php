<?php 
class Database{
  private $bdd;

  CONST DBHOST = 'localhost';
  CONST DBNAME = 'aec_livres';
  CONST DBUSER = 'tp2_aec';
  CONST DBPASSWORD = '123456';
  CONST DBCHARSET = 'utf8mb4';

  const DSN = "mysql:host=" . self::DBHOST. ";dbname=" . self::DBNAME . ";charset=" . self::DBCHARSET;

  CONST OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, 
    PDO::ATTR_EMULATE_PREPARES   => false,
  ];

  public function getPdo()
  {
    $this->bdd = new PDO(self::DSN, self::DBUSER, self::DBPASSWORD, self::OPTIONS);
    return $this->bdd;
  }

}
?>