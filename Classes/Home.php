<?php 
namespace Classes;

/**
 * Classe mère pour gérer les types de maisons (a vendre et a louer)
 */
class Home{
  protected $image, $address, $price;

  public function __construct($address, $image, $price, $date_publication){
    $this->setPrenom($address); 
    $this->setNom($image); 
    $this->setAge($age); 
    $this->setAge($price); 
    $this->setAge($date_publication); 
  }

  /**
   * Get the value of nom
   */
  public function getNom()
  {
    return $this->image;
  }

  /**
   * Set the value of nom
   */
  public function setNom($nom)
  {
    $this->nom = $nom;
  }

  /**
   * Get the value of prenom
   */
  public function getPrenom()
  {
    return $this->prenom;
  }

  /**
   * Set the value of prenom
   */
  protected function setPrenom($prenom)
  {
    $this->prenom = $prenom;
  }

  /**
   * Get the value of age
   */
  public function getAge()
  {
    return $this->age;
  }

  /**
   * Set the value of age
   */
  public function setAge($age)
  {
    $this->age = $age;
  }

  /**
   * Get the value of age
   */
  public function getPrice()
  {
    return $this->price;
  }

  /**
   * Set the value of age
   */
  public function setPrice($price)
  {
    $this->price = $price;
  }

  public function getDate()
  {
    return $this->date_publication;
  }

  /**
   * Set the value of age
   */
  public function setDate($date_publication)
  {
    $this->date_publication = $date_publication;
  }
}

?>