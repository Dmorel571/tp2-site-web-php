<?php 
namespace Classes;

/**
 * Classe pour décrire les professeurs
 */
class Rental extends Home {

  private $id;

  public function __construct($nom, $prenom, $age, $id){
    parent::__construct($nom, $prenom, $age);

    // parent::setPrenom($prenom);
    // parent::setNom($nom);
    // parent::setAge($age);
    $this->setid($id);
  }

  // Exemple de surchage de méthode
  // Exécute cette méthode puis la même méthode de la classe mère
  public function getNom() {
    echo "le nom du prof: ";
    parent::getNom();
  }

  // parent::getPrenom() => Appelle la méthode de la classe mère  
  public function __toString(){
    // return $this->nom;
    return $this->getNom() . " " . parent::getPrenom(). " " . $this->getid();
  }

  /**
   * Get the value of id
   */
  public function getid()
  {
    return $this->id;
  }

  /**
   * Set the value of id
   */
  public function setid($id)
  {
    $this->id = $id;
  }
}
?>