<?php 
namespace Classes;

/**
 * Classe pour décrire les professeurs
 */
class Professeur extends Personne {

  private $nas;

  public function __construct($nom, $prenom, $age, $nas){
    parent::__construct($nom, $prenom, $age);

    // parent::setPrenom($prenom);
    // parent::setNom($nom);
    // parent::setAge($age);
    $this->setNas($nas);
  }

  // Exemple de surchage de méthode
  // Exécute cette méthode puis la même méthode de la classe mère
  public function getNom() {
    echo "le nom du prof: ";
    parent::getNom();
  }

  // parent::getPrenom() => Appelle la méthode de la classe mère  
  public function __toString(){
    // return $this->nom;
    return $this->getNom() . " " . parent::getPrenom(). " " . $this->getNas();
  }

  /**
   * Get the value of nas
   */
  public function getNas()
  {
    return $this->nas;
  }

  /**
   * Set the value of nas
   */
  public function setNas($nas)
  {
    $this->nas = $nas;
  }
}
?>