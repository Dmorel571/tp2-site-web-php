<?php 
require __DIR__.'/vendor/autoload.php';
require 'connexion.php';
use Classes\Rental;
use Classes\Sale;

/* Menu active */
$active = "all";

/* Aller chercher les maisons */
if (isset($_GET['p']) && $_GET['p'] === "all"){
    
}

if (isset($_GET['p']) && $_GET['p'] === "rental"){
    $active = "rental"; 
}

if (isset($_GET['p']) && $_GET['p'] === "sale"){
    $active = "sale";
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Agence</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link <?php ($active == 'all')? "active":"" ?>"  href="?p=all">Tout</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" <?php ($active == 'sale')? "active":"" ?> href="?p=sale">À Vendre</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" <?php ($active == 'rental')? "active":"" ?> href="?p=rental">À Louer</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<?php 
    foreach ($homes as $home) {
        
    }
?>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
  </body>
</html>
